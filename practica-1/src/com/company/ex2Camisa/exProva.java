package com.company.ex2Camisa;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class exProva {
    public static void main(String[] args) {
        ArrayList<camisa> lista=new ArrayList<camisa>();
        lista.add(new camisa("XL", "rojo", "sara"));
        lista.add(new camisa("M", "azul", "chinos"));
        lista.add(new camisa("S", "rojo", "H&M"));
        lista.add(new camisa("XL", "azul", "Levi"));
        lista.add(new camisa("M", "azul", "lululemon"));
        System.out.println("Talls XL");
        prirntTallaXL(lista,"XL");
        System.out.println("color rojo");
        prirntColorRojo(lista,"rojo");
        System.out.println("Talls XL y azul");
        prirnt2Atributos(lista,"azul","XL");
        System.out.println();
        System.out.println("CON LAMBDA:");
        System.out.println("Talls XL");
        ArrayList<camisa> xl= lista.stream()
                .filter(camisa->camisa.getTalla().equals("XL"))
                .collect(Collectors.toCollection(()-> new ArrayList<camisa>()));
        xl.forEach(e->System.out.println(e));
        System.out.println("color rojo");
        ArrayList<camisa> rojo= lista.stream()
                .filter(camisa->camisa.getColor().equals("rojo"))
                .collect(Collectors.toCollection(()-> new ArrayList<camisa>()));
        rojo.forEach(e->System.out.println(e));
        System.out.println("Talls XL y azul");
        ArrayList<camisa> atributos= lista.stream()
                .filter(camisa->camisa.getColor().equals("azul") && camisa.getTalla().equals("XL"))
                .collect(Collectors.toCollection(()-> new ArrayList<camisa>()));
        atributos.forEach(e->System.out.println(e));
    }
    private static void prirntTallaXL(List<camisa>lista,String talla){
        for(camisa c: lista){
            if(c.getTalla().equals(talla)){
                System.out.println(c);
            }

        }
    }
    private static void prirntColorRojo(List<camisa>lista,String color){
        for(camisa c: lista){
            if(c.getColor().equals(color)){
                System.out.println(c);
            }
        }
    }
    private static void prirnt2Atributos(List<camisa>lista,String color,String talla){
        //M-azul
        for(camisa c: lista){
            if(c.getTalla().equals(talla) && c.getColor().equals(color)){
                System.out.println(c);
            }
        }
    }
}
