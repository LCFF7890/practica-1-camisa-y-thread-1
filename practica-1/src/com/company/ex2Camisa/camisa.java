package com.company.ex2Camisa;

public class camisa {
    String talla,color,model;

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public camisa(String talla, String color, String model) {
        this.talla = talla;
        this.color = color;
        this.model = model;
    }

    @Override
    public String toString() {
        return "camisa{" +
                "talla='" + talla + '\'' +
                ", color='" + color + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
