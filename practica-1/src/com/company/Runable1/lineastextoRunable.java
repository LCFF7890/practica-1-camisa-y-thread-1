package com.company.Runable1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class lineastextoRunable extends Thread{
    long lNumeroLineas = 0;
    public lineastextoRunable(String fichero) {
        this.fichero = fichero;
    }
    public void contarTexto(String f) throws IOException {
        FileReader fr = new FileReader("src"+File.separator+"com"+File.separator+"company"+File.separator+"thread1"+File.separator+"textos"+File.separator+f+".txt");
        BufferedReader bf = new BufferedReader(fr);
        while ((bf.readLine())!=null) {
            lNumeroLineas++;
        }
    }
    private String fichero;

    @Override
    public void run() {
        try {
            contarTexto(fichero);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("El fichero tiene " + lNumeroLineas + " lineas");
    }
}
