package com.company.thread1;

import java.io.*;

public class lineastextoThread extends Thread{
    long lNumeroLineas = 0;
    public lineastextoThread(String fichero) {
        this.fichero = fichero;
    }
    public void contarTexto(String f) throws IOException {
        FileReader fr = new FileReader("src"+File.separator+"com"+File.separator+"company"+File.separator+"thread1"+File.separator+"textos"+File.separator+f+".txt");
        BufferedReader bf = new BufferedReader(fr);
        while ((bf.readLine())!=null) {
            lNumeroLineas++;
        }
    }
    private String fichero;

    @Override
    public void run() {
        try {
            contarTexto(fichero);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("El fichero tiene " + lNumeroLineas + " lineas");
        super.run();
    }
}
